/*
	Fun commands for fun times.
	
	Current commands:
	- howgayis
	- 8ball
*/

module.exports = (self) => {
	
	/*
		howgayis, revived, stolen command from shoob. credits to gibs.
		Usage: howgayis {person}
	*/
	
	self.registerCommand('howgayis', function (msg, args) {
		// If no user is specified
		if (!args[0]) return this.send(msg, 'Please mention a person.')
		
		//Generate random number and sends message
		var random = Math.floor((Math.random() * 100) + 1)
		this.self.createMessage(msg.channel.id, args.join(" ") + ' is ' + random + ' gay')
	}, {
		aliases: ['gay']
	})
	
	
	/*
		Standard 8ball command, user asks a question and it will reply with a random message.
		Usage: 8ball {question}
	*/
	
	self.registerCommand('8ball', function (msg, args) {
		// If no question is specified
		if (!args[0]) return this.send(msg, 'Please ask a question.')
		
		//List of potential answers
		var options = [
			"should never hit this",
			"It is certain",
			"It is decidedly so",
			"Without a doubt",
			"Yes – definitely",
			"You may rely on it",
			"As I see it, yes",
			"Most likely",
			"Outlook good",
			"Yes",
			"Signs point to yes",
			"Don’t count on it",
			"My reply is no",
			"My sources say no",
			"Outlook not so good",
			"Very doubtful",
			"Reply hazy, try again",
			"Ask again later",
			"Better not tell you now",
			"Cannot predict now",
			"Concentrate and ask again"
			]
		
		//Generates random number based on the amount of possible answers.
		var num = Math.floor((Math.random() * options.length) + 1)
		
		//Sends message with the random answer.
		this.self.createMessage(msg.channel.id, ':question: ' + args.join(" ") + '\n:8ball: ' + options[num])
	}, {
		deleteAfter: false
	})
}